const functionGetLeftMargin = (leftMargin) => {
    const marginLeftCal1 = window.getComputedStyle(leftMargin).marginLeft;
    return parseInt(marginLeftCal1.replace(/\D/g, ""));
}
const functionGetGapWidth = (innerSlider) => {
    const gapWidth = window.getComputedStyle(innerSlider).gap;
    return parseInt(gapWidth.replace(/\D/g, ""));
}

const slider5 = document.querySelector('.sider_container5'); 
const sliderFiveRight = document.querySelector('.sliderFiveRight');
const sliderFiveLeft = document.querySelector('.sliderFiveLeft');
const card = document.querySelectorAll('.smallerCentreCard');
const sliderDiv = document.querySelector('.slider5 .sider_container');
const innerSlider = document.querySelector('.slider5 .innerSlider');
const marginLeft = document.querySelector('.slider1 .firstChildMargin');
const marginLeftCalNumber = functionGetLeftMargin(marginLeft);
const totalDivWidth = innerSlider.offsetWidth;
const cardWidth = card[0].offsetWidth;
const gapWidthNumber = functionGetGapWidth(innerSlider);

const slider5obj = {
    leftOverRightEnd: 0,
    disableSnapOnScroll: false,
}

sliderFiveRight.addEventListener('click', () => {
   functionForRightClick(slider5obj, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, totalDivWidth);
})

sliderFiveLeft.addEventListener('click', () => {
  functionForLeftClick(slider5obj, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, slider5, gapWidthNumber, cardWidth); 
})

sliderDiv.addEventListener('scroll', () => {
    functionForScroll(slider5obj, sliderFiveLeft, sliderFiveRight, sliderDiv, slider5, totalDivWidth, marginLeftCalNumber);
})

window.addEventListener('resize', () => {
    functionForResize(sliderFiveLeft, sliderFiveRight, sliderDiv, totalDivWidth);
    functionForResize(slider1Left, slider1Right, sliderDiv1, totalDivWidth1)
});




const functionForScroll = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, slider5, totalDivWidth, marginLeftCalNumber) => {
    sliderFiveRight.disabled = false;
    if (sliderDiv.scrollLeft < marginLeftCalNumber) {
        sliderFiveLeft.disabled = true;
        slider5.classList.remove('snapEffect');
    } else if (sliderDiv.scrollLeft > (totalDivWidth - sliderDiv.getBoundingClientRect().width) - 30) {
        sliderFiveRight.disabled = true;
    } else {
       /*  slider5.classList.add('snapEffect'); */
        sliderFiveLeft.disabled = false;
        sliderFiveRight.disabled = false;
        if(!sliderToChange.disableSnapOnScroll){
            slider5.classList.add('snapEffect');
        }
    }
}


const functionForResize = (sliderFiveLeft, sliderFiveRight, sliderDiv, totalDivWidth) => {
    if (sliderDiv.scrollLeft < 100) {
        sliderFiveLeft.disabled = true;
    } else if (sliderDiv.scrollLeft > (totalDivWidth - sliderDiv.getBoundingClientRect().width) - 30) {
        sliderFiveRight.disabled = true;
    } else {
        sliderFiveLeft.disabled = false;
        sliderFiveRight.disabled = false;
    }
}


const functionForLeftClick = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, slider5, gapWidthNumber, cardWidth) => {
    sliderToChange.disableSnapOnScroll = true;
    sliderDiv.classList.remove('snapEffect');
    sliderFiveRight.disabled = false;
    let count = 0;
    let leftOver = 0;

    if (sliderDiv.scrollLeft < marginLeftCalNumber) {
        leftOver = marginLeftCalNumber - sliderDiv.scrollLeft;
    }
 
    sliderDiv.scrollLeft = sliderDiv.scrollLeft + leftOver;

    const scrollEfect = setInterval(() => {
        if (sliderToChange.leftOverRightEnd > 0) {
            sliderDiv.scrollLeft = sliderDiv.scrollLeft - 6;
            sliderToChange.leftOverRightEnd = sliderToChange.leftOverRightEnd - 6;
        } else {
            sliderDiv.scrollLeft = sliderDiv.scrollLeft - 6;
            count = count + 6;
        }
        if (sliderDiv.scrollLeft <= 0) {
            clearInterval(scrollEfect);
            sliderFiveLeft.disabled = true;
            count = 0;
            slider5.classList.add('snapEffect');
            sliderToChange.disableSnapOnScroll = false;
        } else if (count >= cardWidth + gapWidthNumber) {
            clearInterval(scrollEfect);
            count = 0;
            slider5.classList.add('snapEffect');
            sliderToChange.disableSnapOnScroll = false;
        }
    }, 1);
}

const functionForRightClick = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, totalDivWidth) => {
    sliderDiv.classList.remove('snapEffect');
    sliderToChange.disableSnapOnScroll = true;
    sliderFiveLeft.disabled = false;
    let leftOver = 0;
    let count = 0;

    if (sliderDiv.scrollLeft < marginLeftCalNumber) {
        leftOver = marginLeftCalNumber - sliderDiv.scrollLeft;
    }

    sliderDiv.scrollLeft = sliderDiv.scrollLeft + leftOver;

    const scrollEfect = setInterval(() => {
        sliderDiv.scrollLeft = sliderDiv.scrollLeft + 6;
        count = count + 6;

        if (sliderDiv.scrollLeft >= totalDivWidth - sliderDiv.getBoundingClientRect().width) {
            clearInterval(scrollEfect);
            sliderFiveRight.disabled = true;
            sliderToChange.leftOverRightEnd = count;
            slider5.classList.add('snapEffect');
            count = 0;
            sliderToChange.disableSnapOnScroll = false;

        } else if (count >= cardWidth + gapWidthNumber) {
            clearInterval(scrollEfect);
            count = 0;
            slider5.classList.add('snapEffect');
            sliderToChange.disableSnapOnScroll = false;
        }
    }, 1);
}







const slider1 = document.querySelector('.sider_container1'); 
const slider1Right = document.querySelector('.slider1 .slider1Right');
const slider1Left = document.querySelector('.slider1 .slider1Left');
const card1 = document.querySelectorAll('.slider1 .smallerCentreCard');
const sliderDiv1 = document.querySelector('.slider1 .sider_container');
const innerSlider1 = document.querySelector('.slider1 .innerSlider');
const marginLeft1 = document.querySelector('.slider1 .firstChildMargin');
const marginLeftCalNumber1 = functionGetLeftMargin(marginLeft1);
const totalDivWidth1 = innerSlider1.offsetWidth;
const cardWidth1 = card1[0].offsetWidth;
const gapWidthNumber1 = functionGetGapWidth(innerSlider1);

const slider1obj = {
    leftOverRightEnd: 0,
    disableSnapOnScroll: false,
}
slider1Right.addEventListener('click', () => {
   functionForRightClick(slider1obj, slider1Left, slider1Right, sliderDiv1, marginLeftCalNumber1, totalDivWidth1);
})
slider1Left.addEventListener('click', () => {
  functionForLeftClick(slider1obj, slider1Left, slider1Right, sliderDiv1, marginLeftCalNumber1, slider1, gapWidthNumber1, cardWidth1); 
})
sliderDiv1.addEventListener('scroll', () => {
    functionForScroll(slider1obj, slider1Left, slider1Right, sliderDiv1, slider1, totalDivWidth1, marginLeftCalNumber1);
})